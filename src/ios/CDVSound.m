/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

#import "CDVSound.h"
#import <AVFoundation/AVFoundation.h>
#include <math.h>

#define DOCUMENTS_SCHEME_PREFIX @"documents://"
#define HTTP_SCHEME_PREFIX @"http://"
#define HTTPS_SCHEME_PREFIX @"https://"
#define CDVFILE_PREFIX @"cdvfile://"

@implementation CDVSound

BOOL keepAvAudioSessionAlwaysActive = NO;

@synthesize soundCache, avSession, currMediaId, statusCallbackId;

-(void) pluginInitialize
{
    NSDictionary* settings = self.commandDelegate.settings;
    keepAvAudioSessionAlwaysActive = [[settings objectForKey:[@"KeepAVAudioSessionAlwaysActive" lowercaseString]] boolValue];
    avPlayer.automaticallyWaitsToMinimizeStalling = YES;
    if (keepAvAudioSessionAlwaysActive) {
        if ([self hasAudioSession]) {
            NSError* error = nil;
            if(![self.avSession setActive:YES error:&error]) {
                NSLog(@"Unable to activate session: %@", [error localizedFailureReason]);
            }
        }
    }
}

// Maps a url for a resource path for playing
// "Naked" resource paths are assumed to be from the www folder as its base
- (NSURL*)urlForPlaying:(NSString*)resourcePath
{
    if ([resourcePath hasPrefix:HTTP_SCHEME_PREFIX] || [resourcePath hasPrefix:HTTPS_SCHEME_PREFIX]) {
        NSURL* resourceURL = [NSURL URLWithString:resourcePath];
        NSLog(@"Will use resource from the Internet. '%@' ", resourcePath);
        return resourceURL;
    }
    NSString* filePath = nil;
    if ([resourcePath hasPrefix:DOCUMENTS_SCHEME_PREFIX]) {
        NSString* docsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        filePath = [resourcePath stringByReplacingOccurrencesOfString:DOCUMENTS_SCHEME_PREFIX withString:[NSString stringWithFormat:@"%@/", docsPath]];
        NSLog(@"Will use resource from the documents folder with path ='%@', %@", resourcePath, filePath);
    } else {
        // attempt to find file path in www directory or LocalFileSystem.TEMPORARY directory
        filePath = [self.commandDelegate pathForResource:resourcePath];
        if (filePath == nil) {
            // see if this exists in the documents/temp directory from a previous recording
            NSString* testPath = [NSString stringWithFormat:@"%@/%@", [NSTemporaryDirectory()stringByStandardizingPath], resourcePath];
            if ([[NSFileManager defaultManager] fileExistsAtPath:testPath]) {
                // inefficient as existence will be checked again below but only way to determine if file exists from previous recording
                filePath = testPath;
                NSLog(@"Will attempt to use file resource from LocalFileSystem.TEMPORARY directory");
            } else {
                // attempt to use path provided
                filePath = resourcePath;
                NSLog(@"Will attempt to use file resource '%@'", filePath);
            }
        } else {
            NSLog(@"Found resource '%@' in the web folder.", filePath);
        }
    }
    // if the resourcePath resolved to a file path, check that file exists
    if (filePath == nil) return nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSLog(@"Unknown resource '%@'", resourcePath);
        return nil;
    }
    return [NSURL fileURLWithPath:filePath];
}

// Creates or gets the cached audio file resource object
- (CDVAudioFile*)audioFileForResource:(NSString*)resourcePath withId:(NSString*)mediaId doValidation:(BOOL)bValidate suppressValidationErrors:(BOOL)bSuppress
{
    CDVAudioFile* audioFile = nil;
     if ([self soundCache] == nil) {
           [self setSoundCache:[NSMutableDictionary dictionaryWithCapacity:1]];
       } else {
           audioFile = [[self soundCache] objectForKey:mediaId];
       }
    
    if (audioFile == nil) {
        // validate resourcePath and create
        if ((resourcePath == nil) || ![resourcePath isKindOfClass:[NSString class]] || [resourcePath isEqualToString:@""]) {
            [self onStatus:MEDIA_ERROR mediaId:mediaId param:[self createMediaErrorWithCode:MEDIA_ERR_ABORTED message:@"invalid media src argument"]];
            return audioFile;
        } else {
            audioFile = [[CDVAudioFile alloc] init];
            audioFile.resourcePath = resourcePath;
            audioFile.resourceURL = nil;  // validate resourceURL when actually play or record
            [[self soundCache] setObject:audioFile forKey:mediaId];
        }
    }
    if (bValidate && (audioFile.resourceURL == nil)) {
        NSURL* resourceURL = [self urlForPlaying:resourcePath];
        audioFile.resourceURL = resourceURL;
        if (resourceURL == nil) {
            audioFile.resourceURL = nil;
            NSString* errMsg = [NSString stringWithFormat:@"Cannot use audio file from resource '%@'", resourcePath];
            [self onStatus:MEDIA_ERROR mediaId:mediaId param:[self createMediaErrorWithCode:MEDIA_ERR_ABORTED message:errMsg]];
        }
    }

    return audioFile;
}

// returns whether or not audioSession is available - creates it if necessary
- (BOOL)hasAudioSession
{
    if (!self.avSession) self.avSession = [AVAudioSession sharedInstance];
    return YES;
}

// helper function to create a error object string
- (NSDictionary*)createMediaErrorWithCode:(CDVMediaError)code message:(NSString*)message
{
    NSMutableDictionary* errorDict = [NSMutableDictionary dictionaryWithCapacity:2];

    [errorDict setObject:[NSNumber numberWithUnsignedInteger:code] forKey:@"code"];
    [errorDict setObject:message ? message:@"" forKey:@"message"];
    return errorDict;
}

//helper function to create specifically an abort error
-(NSDictionary*)createAbortError:(NSString*)message
{
  return [self createMediaErrorWithCode:MEDIA_ERR_ABORTED message:message];
}

- (void)create:(CDVInvokedUrlCommand*)command
{
        NSString* mediaId = [command argumentAtIndex:0];
        NSString* resourcePath = [command argumentAtIndex:1];
        NSDictionary* options = [command argumentAtIndex:2 withDefault:nil];
        self.currMediaId = mediaId;
    
        if (options && [options objectForKey:@"CloudFront-Key-Pair-Id"]) {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:[NSHTTPCookie cookieWithProperties: [NSDictionary dictionaryWithObjectsAndKeys:
                                                                 @"CloudFront-Key-Pair-Id", NSHTTPCookieName,
                                                                  [options valueForKey:@"CloudFront-Key-Pair-Id"], NSHTTPCookieValue,
                                                                 @"TRUE", NSHTTPCookieSecure,
                                                                 @"/", NSHTTPCookiePath,
                                                                [options valueForKey:@"CloudFront-Domain"], NSHTTPCookieDomain, nil]]];
        }
        if (options && [options objectForKey:@"CloudFront-Signature"]) {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:[NSHTTPCookie cookieWithProperties: [NSDictionary dictionaryWithObjectsAndKeys:
                                                                 @"CloudFront-Signature", NSHTTPCookieName,
                                                                  [options valueForKey:@"CloudFront-Signature"], NSHTTPCookieValue,
                                                                 @"TRUE", NSHTTPCookieSecure,
                                                                 @"/", NSHTTPCookiePath,
                                                                [options valueForKey:@"CloudFront-Domain"], NSHTTPCookieDomain, nil]]];
        }
        if (options && [options objectForKey:@"CloudFront-Policy"]) {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:[NSHTTPCookie cookieWithProperties: [NSDictionary dictionaryWithObjectsAndKeys:
                                                                 @"CloudFront-Policy", NSHTTPCookieName,
                                                                  [options valueForKey:@"CloudFront-Policy"], NSHTTPCookieValue,
                                                                 @"TRUE", NSHTTPCookieSecure,
                                                                 @"/", NSHTTPCookiePath,
                                                                [options valueForKey:@"CloudFront-Domain"], NSHTTPCookieDomain, nil]]];
        }
    
        CDVAudioFile* audioFile = [self audioFileForResource:resourcePath withId:mediaId doValidation:YES suppressValidationErrors:YES];
        if (audioFile == nil) {
            return [self onStatus:MEDIA_ERROR mediaId:mediaId param:[self createAbortError:[NSString stringWithFormat:@"Failed to initialize %@", resourcePath]]];
        }

        BOOL isLocalFile = [audioFile.resourceURL isFileURL] || [resourcePath hasPrefix:CDVFILE_PREFIX] || [resourcePath hasPrefix:DOCUMENTS_SCHEME_PREFIX];
        if (!isLocalFile) {
            // Creating playerItem instance - 08-02-2021 Changing from playerItemWithURL to playerItemWithAsset
            NSLog(@"cookies: %@",[[NSHTTPCookieStorage sharedHTTPCookieStorage]cookies]);
            AVURLAsset* asset = [AVURLAsset URLAssetWithURL:audioFile.resourceURL options:@{AVURLAssetHTTPCookiesKey: [[NSHTTPCookieStorage sharedHTTPCookieStorage]cookies] }];
            AVPlayerItem* playerItem = [AVPlayerItem playerItemWithAsset:asset];

            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemStalledPlaying:) name:AVPlayerItemPlaybackStalledNotification object:playerItem];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemFailedPlaying:) name:AVPlayerItemFailedToPlayToEndTimeErrorKey object:playerItem];
            // Pass the AVPlayerItem to a new player. will not be replaced anymore, instead a new instance will be created always
            audioFile.avPlayerInstance = [[AVPlayer alloc] initWithPlayerItem:playerItem];
        }
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
}

- (void)setVolume:(CDVInvokedUrlCommand*)command
{[self.commandDelegate runInBackground:^{
        NSString* callbackId = command.callbackId;
    #pragma unused(callbackId)
        NSString* mediaId = [command argumentAtIndex:0];
        NSNumber* volume = [command argumentAtIndex:1 withDefault:[NSNumber numberWithFloat:1.0]];

    if ([self soundCache] == nil) return;
    CDVAudioFile* audioFile = [[self soundCache] objectForKey:mediaId];
    if (audioFile == nil) return;
    audioFile.volume = volume;
    if (audioFile.player) {
        audioFile.player.volume = [volume floatValue];
    }
    else {
        float customVolume = [volume floatValue];
        if (customVolume >= 0.0 && customVolume <= 1.0) {
            [audioFile.avPlayerInstance setVolume: customVolume];
        }
        else {
            NSLog(@"The value must be within the range of 0.0 to 1.0");
        }
    }
    [[self soundCache] setObject:audioFile forKey:mediaId];
}];
}

- (void)setRate:(CDVInvokedUrlCommand*)command
{
    NSString* mediaId = [command argumentAtIndex:0];

    if ([self soundCache] == nil) return;
    CDVAudioFile* audioFile = [[self soundCache] objectForKey:mediaId];
    if (audioFile == nil) return;
    
    NSNumber* rate = [command argumentAtIndex:1 withDefault:[NSNumber numberWithFloat:1.0]];
    audioFile.rate = rate;
    //Local File
    if (audioFile.player) {
        audioFile.player.enableRate = YES;
        audioFile.player.rate = [rate floatValue];
    }
    //URL File
    else if (audioFile.avPlayerInstance.currentItem && audioFile.avPlayerInstance.currentItem.asset){
        [audioFile.avPlayerInstance setRate:[rate floatValue]];
    }

    [[self soundCache] setObject:audioFile forKey:mediaId];
}

- (void)startPlayingAudio:(CDVInvokedUrlCommand*)command
{
[self.commandDelegate runInBackground:^{
    NSString* mediaId = [command argumentAtIndex:0];
    NSString* resourcePath = [command argumentAtIndex:1];
    NSDictionary* options = [command argumentAtIndex:2 withDefault:nil];
    self.currMediaId = mediaId;

    CDVAudioFile* audioFile = [self audioFileForResource:resourcePath withId:mediaId doValidation:YES suppressValidationErrors:NO];
    if (audioFile == nil && audioFile.resourceURL == nil) return NSLog(@"no audioFile nor resourceUrl. Returning");
        
    if (audioFile.player == nil) {
        BOOL notPrepared = [self prepareToPlay:audioFile withId:mediaId];
        if (notPrepared) {
            return [self onStatus:MEDIA_ERROR mediaId:mediaId param:[self createMediaErrorWithCode:MEDIA_ERR_NONE_SUPPORTED message:@"Error on prepareToPlay method"]];
        }
    }
    
     [self onStatus:MEDIA_STATE mediaId:mediaId param:@(MEDIA_STARTING)];
    // get the audioSession and set the category to allow Playing when device is locked or ring/silent switch engaged
    if ([self hasAudioSession]) {
        NSError* __autoreleasing err = nil;
        
        if ([options objectForKey:@"automaticallyWaitsToMinimizeStalling"] != nil && [[options objectForKey:@"automaticallyWaitsToMinimizeStalling"] boolValue] == NO) {
            audioFile.avPlayerInstance.automaticallyWaitsToMinimizeStalling = NO;
        } else {
            audioFile.avPlayerInstance.automaticallyWaitsToMinimizeStalling = YES;
        }
        
        if ([options objectForKey:@"playAudioWhenScreenIsLocked"] != nil && [[options objectForKey:@"playAudioWhenScreenIsLocked"] boolValue] == NO) {
            [self.avSession setCategory:AVAudioSessionCategorySoloAmbient error:&err];
        }
        else {
            [self.avSession setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionAllowBluetoothA2DP error:&err];
        }
        
        if (![self.avSession setActive:YES error:&err]) {
            // other audio with higher priority that does not allow mixing could cause this to fail
            NSLog(@"Unable to play audio: %@", [err localizedFailureReason]);
            [self onStatus:MEDIA_ERROR mediaId:mediaId param:[self createMediaErrorWithCode:MEDIA_ERR_DECODE message:@"Cannot get Session Active"]];
            return;
        }
    }
    //audioSession created and can play
    NSLog(@"Start Playing audio sample '%@'", audioFile.resourcePath);
    double duration = 0;
    if (audioFile.avPlayerInstance.currentItem && audioFile.avPlayerInstance.currentItem.asset) {
        CMTime time = audioFile.avPlayerInstance.currentItem.asset.duration;
        duration = CMTimeGetSeconds(time);
        if (isnan(duration)) duration = -1;

        if (audioFile.rate != nil){
            float customRate = [audioFile.rate floatValue];
            NSLog(@"Playing stream with AVPlayer & custom rate");
            [audioFile.avPlayerInstance setRate:customRate];
        } else {
            NSLog(@"Playing stream with AVPlayer & default rate");
            [audioFile.avPlayerInstance play];
        }
    }
    //LocalFile
    else {
        NSNumber* loopOption = [options objectForKey:@"numberOfLoops"];
        NSInteger numberOfLoops = 0;
        if (loopOption != nil) numberOfLoops = [loopOption intValue] - 1;
        audioFile.player.numberOfLoops = numberOfLoops;
        if (audioFile.player.isPlaying) {
            [audioFile.player stop];
            audioFile.player.currentTime = 0;
        }
        if (audioFile.volume != nil) {
            audioFile.player.volume = [audioFile.volume floatValue];
        }

        audioFile.player.enableRate = YES;
        if (audioFile.rate != nil) {
            audioFile.player.rate = [audioFile.rate floatValue];
        }
        //finally play and return status
        [audioFile.player play];
        duration = round(audioFile.player.duration * 1000) / 1000;
    }

    [self onStatus:MEDIA_DURATION mediaId:mediaId param:@(duration)];
    [self onStatus:MEDIA_STATE mediaId:mediaId param:@(MEDIA_RUNNING)];
}];
}

- (BOOL)prepareToPlay:(CDVAudioFile*)audioFile withId:(NSString*)mediaId
{
    NSError* __autoreleasing playerError = nil;
    NSURL* resourceURL = audioFile.resourceURL;
    //when is local file, we use audioFile.player. instead we use avPlayerInstance
    
    if ([resourceURL isFileURL]) {
        audioFile.player = [[CDVAudioPlayer alloc] initWithContentsOfURL:resourceURL error:&playerError];
    }

    if (playerError != nil) {
        NSLog(@"Failed to initialize AVAudioPlayer: %@\n", [playerError localizedDescription]);
        audioFile.player = nil;
        return YES;
    }
    else {
        audioFile.player.mediaId = mediaId;
        audioFile.player.delegate = self;
        if (audioFile.avPlayerInstance == nil) return ![audioFile.player prepareToPlay];
    }
    return NO;
}

- (void)stopPlayingAudio:(CDVInvokedUrlCommand*)command
{
    NSString* mediaId = [command argumentAtIndex:0];
    CDVAudioFile* audioFile = [[self soundCache] objectForKey:mediaId];
    if (audioFile == nil) return;
    
    if (audioFile.avPlayerInstance.currentItem && audioFile.avPlayerInstance.currentItem.asset) {
        [audioFile.avPlayerInstance pause];
        BOOL isReadyToSeek = (audioFile.avPlayerInstance.status == AVPlayerStatusReadyToPlay);
        if (isReadyToSeek) {
            [audioFile.avPlayerInstance seekToTime: kCMTimeZero
                 toleranceBefore: kCMTimeZero
                  toleranceAfter: kCMTimeZero
                                 completionHandler: ^(BOOL finished){ [audioFile.avPlayerInstance pause]; }];
            [self onStatus:MEDIA_STATE mediaId:mediaId param:@(MEDIA_STOPPED)];
        }
    }
    else if (audioFile.player != nil) {
        NSLog(@"Stopped playing audio sample '%@'", audioFile.resourcePath);
        [audioFile.player stop];
        audioFile.player.currentTime = 0;
        [self onStatus:MEDIA_STATE mediaId:mediaId param:@(MEDIA_STOPPED)];
    }
}

- (void)pausePlayingAudio:(CDVInvokedUrlCommand*)command
{
    NSString* mediaId = [command argumentAtIndex:0];
    CDVAudioFile* audioFile = [[self soundCache] objectForKey:mediaId];

    if (audioFile == nil) return;
    if (audioFile.player != nil) {
        [audioFile.player pause];
        [self onStatus:MEDIA_STATE mediaId:mediaId param:@(MEDIA_PAUSED)];
    }
    else if (audioFile.avPlayerInstance != nil) {
        [audioFile.avPlayerInstance pause];
        [self onStatus:MEDIA_STATE mediaId:mediaId param:@(MEDIA_PAUSED)];
    }
    NSLog(@"Paused playing audio sample '%@'", audioFile.resourcePath);
}

- (void)seekToAudio:(CDVInvokedUrlCommand*)command
{
[self.commandDelegate runInBackground:^{
    // args: 0 = Media id 1 = seek to location in milliseconds
    NSString* mediaId = [command argumentAtIndex:0];

    CDVAudioFile* audioFile = [[self soundCache] objectForKey:mediaId];
    double position = [[command argumentAtIndex:1] doubleValue];
    double posInSeconds = position / 1000;

    if (audioFile == nil) return;
    //Local Files
    if (audioFile.player != nil) {
        if (posInSeconds >= audioFile.player.duration) {
            // The seek is past the end of file.  Stop media and reset to beginning instead of seeking past the end.
            [audioFile.player stop];
            audioFile.player.currentTime = 0;
            [self onStatus:MEDIA_STATE mediaId:mediaId param:@(MEDIA_STOPPED)];
        } else {
            audioFile.player.currentTime = posInSeconds;
            [self onStatus:MEDIA_POSITION mediaId:mediaId param:@(posInSeconds)];
        }
        return;
    }
    
    if (audioFile.avPlayerInstance != nil) {
        int32_t timeScale = audioFile.avPlayerInstance.currentItem.asset.duration.timescale;
        CMTime timeToSeek = CMTimeMakeWithSeconds(posInSeconds, timeScale);

        BOOL isPlaying = audioFile.avPlayerInstance.rate > 0;
        BOOL isReadyToSeek = audioFile.avPlayerInstance.status == AVPlayerStatusReadyToPlay;

        // CB-10535:
        // When dealing with remote files, we can get into a situation where we start playing before AVPlayer has had the time to buffer the file to be played.
        // To avoid such a situation, we only seek if both the player and the player item are ready to play. If not ready, we send an error back to JS land.
        if(isReadyToSeek) {
            [audioFile.avPlayerInstance seekToTime: timeToSeek
                 toleranceBefore: kCMTimeZero
                  toleranceAfter: kCMTimeZero
               completionHandler: ^(BOOL finished) {
                   if (isPlaying) [audioFile.avPlayerInstance play];
               }];
        } else {
            NSString* errMsg = @"AVPlayerItem cannot service a seek request with a completion handler until its status is AVPlayerStatusReadyToPlay.";
            [self onStatus:MEDIA_ERROR mediaId:mediaId param:[self createAbortError:errMsg]];
        }
    }
}];
}


- (void)release:(CDVInvokedUrlCommand*)command
{
[self.commandDelegate runInBackground:^{
    NSString* callbackId = command.callbackId;
    NSString* mediaId = [command argumentAtIndex:0];
    if (mediaId == nil) return;

    CDVAudioFile* audioFile = [[self soundCache] objectForKey:mediaId];
    if (audioFile == nil) return;

    //remote file
    if (audioFile.avPlayerInstance != nil) {
        [audioFile.avPlayerInstance pause];
        audioFile.avPlayerInstance = nil;
    }
    //local file
    else if (audioFile.player && [audioFile.player isPlaying]) {
        [audioFile.player stop];
    }

    NSLog(@"Media with id %@ released", mediaId);
    [[self soundCache] removeObjectForKey:mediaId];
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:callbackId];
}];
}

- (void)getCurrentPositionAudio:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        NSString* callbackId = command.callbackId;
        NSString* mediaId = [command argumentAtIndex:0];

    #pragma unused(mediaId)
        CDVAudioFile* audioFile = [[self soundCache] objectForKey:mediaId];
        if (audioFile == nil) return;
        double position = -1;

        if (audioFile.player != nil && [audioFile.player isPlaying]) {
            position = round(audioFile.player.currentTime * 1000) / 1000;
        }
        else if (audioFile.avPlayerInstance) {
           CMTime time = [audioFile.avPlayerInstance currentTime];
           position = CMTimeGetSeconds(time);
        }

        if (isnan(position)) position = -1;

        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDouble:position];

        [self onStatus:MEDIA_POSITION mediaId:mediaId param:@(position)];
        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
    }];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer*)player successfully:(BOOL)flag
{
    //commented as unused
    CDVAudioPlayer* aPlayer = (CDVAudioPlayer*)player;
    NSString* mediaId = aPlayer.mediaId;
    CDVAudioFile* audioFile = [[self soundCache] objectForKey:mediaId];
    if (audioFile == nil) return;
    
    NSLog(@"Finished playing audio sample '%@'", audioFile.resourcePath);
    if (flag) {
        audioFile.player.currentTime = 0;
        [self onStatus:MEDIA_STATE mediaId:mediaId param:@(MEDIA_ENDED)];
    } else {
        [self onStatus:MEDIA_ERROR mediaId:mediaId param:[self createMediaErrorWithCode:MEDIA_ERR_DECODE message:nil]];
    }
     if (!keepAvAudioSessionAlwaysActive && self.avSession && ! [self isPlayingOrRecording]) {
         [self.avSession setActive:NO error:nil];
     }
}

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    // Will be called when AVPlayer finishes playing playerItem
    [self onStatus:MEDIA_STATE mediaId:self.currMediaId param:@(MEDIA_ENDED)];
}

-(void)itemStalledPlaying:(NSNotification *) notification {
    // Will be called when playback stalls due to buffer empty
    NSLog(@"Stalled playback");
    [self onStatus:MEDIA_ERROR mediaId:self.currMediaId param:[self createAbortError:@"stalled_playback"]];
}

-(void)itemFailedPlaying:(NSNotification *) notification {
    NSLog(@"Failed playback");
    [self onStatus:MEDIA_ERROR mediaId:self.currMediaId param:[self createAbortError:@"failed_playback"]];
}

- (void)onMemoryWarning
{
    /* https://issues.apache.org/jira/browse/CB-11513 */
    NSMutableArray* keysToRemove = [[NSMutableArray alloc] init];
    
    for(id key in [self soundCache]) {
        CDVAudioFile* audioFile = [[self soundCache] objectForKey:key];
        if (audioFile != nil) {
            if (audioFile.player != nil && ![audioFile.player isPlaying]) [keysToRemove addObject:key];
            if (audioFile.avPlayerInstance && audioFile.avPlayerInstance.rate == 0) [keysToRemove addObject:key];
        }
    }
    
    [[self soundCache] removeObjectsForKeys:keysToRemove];
    [self setAvSession:nil];
    [super onMemoryWarning];
}


- (void)dealloc
{
    [[self soundCache] removeAllObjects];
}

- (void)onReset
{
    for (CDVAudioFile* audioFile in [[self soundCache] allValues]) {
        if (audioFile != nil) {
            if (audioFile.player != nil) {
                [audioFile.player stop];
                audioFile.player.currentTime = 0;
            }
        }
    }

    [[self soundCache] removeAllObjects];
}

- (void)messageChannel:(CDVInvokedUrlCommand*)command
{
    self.statusCallbackId = command.callbackId;
}

- (void)onStatus:(CDVMediaMsg)what mediaId:(NSString*)mediaId param:(NSObject*)param
{
    if (self.statusCallbackId!=nil) { //new way, android,windows compatible
        NSMutableDictionary* status=[NSMutableDictionary dictionary];
        status[@"msgType"] = @(what);
        //in the error case contains a dict with "code" and "message"
        //otherwise a NSNumber
        status[@"value"] = param;
        status[@"id"] = mediaId;
        NSMutableDictionary* dict=[NSMutableDictionary dictionary];
        dict[@"action"] = @"status";
        dict[@"status"] = status;
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dict];
        [result setKeepCallbackAsBool:YES]; //we keep this forever
        [self.commandDelegate sendPluginResult:result callbackId:self.statusCallbackId];
    } else { //old school evalJs way
        if (what==MEDIA_ERROR) {
            NSData* jsonData = [NSJSONSerialization dataWithJSONObject:param options:0 error:nil];
            param=[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSString* jsString = [NSString stringWithFormat:@"%@(\"%@\",%d,%@);",
              @"cordova.require('cordova-plugin-media.Media').onStatus",
              mediaId, (int)what, param];
        [self.commandDelegate evalJs:jsString];
    }
}

-(BOOL) isPlayingOrRecording
{
    for(NSString* mediaId in soundCache) {
        CDVAudioFile* audioFile = [soundCache objectForKey:mediaId];
        if (audioFile.player && [audioFile.player isPlaying]) return true;
        if (audioFile.avPlayerInstance && audioFile.avPlayerInstance.rate > 0) return true;
    }
    return false;
}

@end

@implementation CDVAudioFile

@synthesize resourcePath;
@synthesize resourceURL;
@synthesize player, volume, rate;

@end
@implementation CDVAudioPlayer
@synthesize mediaId;

@end
